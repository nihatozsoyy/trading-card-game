import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Deck.css';

export default class Player extends Component {
    static propTypes = {
        remainingCardCount: PropTypes.number
    };

    render() {
        return (
            <div className="deckContainer">
                <p>DECK</p>
                <p>Remaining Cards: {this.props.remainingCardCount}</p>
            </div>
        );
    }
}