import React, { Component } from 'react';
import PropTypes from "prop-types";
import './PlayerStats.css';
import Health from '../Health/Health';
import Mana from '../Mana/Mana';

export default class PlayerStats extends Component {
    static propTypes = {
        health: PropTypes.number,
        activeMana: PropTypes.number,
        totalMana: PropTypes.number
    };

    render() {
        const { health, activeMana, totalMana } = this.props;

        return (
            <div className="playerStatsContainer">
                <Health health={health} />
                <Mana activeMana={activeMana} totalMana={totalMana} />
            </div>
        );
    }
}