import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Card.css';

export default class Card extends Component {
    static propTypes = {
        value: PropTypes.number,
        playCard: PropTypes.func,
        isDisabled: PropTypes.bool
    };

    render() {
        const { value, playCard, isDisabled } = this.props;
        return (
            <button className="cardContainer" onClick={() => playCard(value)} disabled={isDisabled}>
                <p>{value}</p>
            </button>
        );
    }
}