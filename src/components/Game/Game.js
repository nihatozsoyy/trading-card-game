import React, { Component } from 'react';
import './Game.css';
import Player from '../Player/Player';
import GameStatus from '../GameStatus/GameStatus';
import initializeGame from '../../logic/initiliazeGame';
import * as processGame from '../../logic/processGame';

export default class Game extends Component {
    constructor(props) {
        super(props);
        this.state = initializeGame(); //initiliazes game with default values
    }

    componentDidMount() {
        this.setState(processGame.startGame(this.state), () => {
            this.isActivePlayerComputer(this.state);
        });
    }

    playCard = (cardValue) => {
        this.setState(processGame.playCard(this.state, cardValue), () => {
            this.isActivePlayerComputer(this.state);
        });
    }

    changeActivePlayer = () => {
        this.setState(processGame.changeActivePlayer(this.state), () => {
            this.isActivePlayerComputer(this.state);
        });
    }

    isActivePlayerComputer = (state) => {
        console.log('aktif player kontrol ediliyor', state.activePlayerName);
        if (state.activePlayerName === 'computer') {
            this.setState(processGame.playComputer(state));
        }
    }

    render() {
        const { user, computer, activePlayerName, winner } = this.state;

        return (
            <div className="gameContainer">
                <div className='gamePart'>
                    <Player player={computer} playCard={(cardValue) => this.playCard(cardValue)} />
                </div>
                <div className='gamePart'>
                    <GameStatus changeActivePlayer={this.changeActivePlayer} activePlayerName={activePlayerName} winner={winner} />
                </div>
                <div className='gamePart'>
                    <Player player={user} playCard={(cardValue) => this.playCard(cardValue)} />
                </div>
            </div>
        );
    }
}
