import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Mana.css';

export default class Mana extends Component {
    static propTypes = {
        activeMana: PropTypes.number,
        totalMana: PropTypes.number
    };

    render() {
        const { activeMana, totalMana } = this.props;
        return (
            <div>
                <p>Mana: {activeMana} / {totalMana}</p>
            </div>
        );
    }
}