import React, { Component } from 'react';
import PropTypes from "prop-types";
import './PlayerHand.css';
import Card from '../Card/Card';

export default class PlayerHand extends Component {
    static propTypes = {
        hand: PropTypes.array,
        playCard: PropTypes.func,
        cardsDisabled: PropTypes.bool
    };

    render() {
        const { hand, playCard, cardsDisabled } = this.props;

        return (
            <div className="playerHandContainer">
                <div className='outerSpace'></div>
                {hand.map((cardValue, index) => (
                    <div className='cardContainer' key={index}>
                        <div className='innerSpace'></div>
                        <div className='card'>
                            <Card
                                value={cardValue}
                                playCard={(cardValue) => playCard(cardValue)}
                                isDisabled={cardsDisabled}
                            />
                        </div>
                        <div className='innerSpace'></div>
                    </div>
                ))}
                <div className='outerSpace'></div>
            </div>
        );
    }
}