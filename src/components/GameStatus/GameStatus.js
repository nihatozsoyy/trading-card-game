import React, { Component } from 'react';
import PropTypes from "prop-types";
import './GameStatus.css';
import Button from '../Button/Button';

export default class GameStatus extends Component {
  static propTypes = {
    activePlayerName: PropTypes.string,
    changeActivePlayer: PropTypes.func,
    winner: PropTypes.string
  };

  render() {
    const { activePlayerName, changeActivePlayer, winner } = this.props;

    if (winner) {
      return (
        <div className="gameStatusContainer">
          <p>Winner: {winner}</p>
        </div>
      );
    } else {
      return (
        <div className="gameStatusContainer">
          <p>Active Player: {activePlayerName}</p>
          <Button text={'End Turn'} clickHandler={changeActivePlayer} isDisabled={activePlayerName === 'computer'} />
        </div>
      );
    }
  }
}