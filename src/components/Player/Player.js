import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Player.css';
import Deck from '../Deck/Deck';
import PlayerHand from '../PlayerHand/PlayerHand';
import PlayerStats from '../PlayerStats/PlayerStats';

export default class Player extends Component {
    static propTypes = {
        player: PropTypes.shape({
            health: PropTypes.number,
            activeMana: PropTypes.number,
            totalMana: PropTypes.number,
            deck: PropTypes.array,
            hand: PropTypes.array,
            cardsDisabled: PropTypes.bool
        }),
        playCard: PropTypes.func
    };

    render() {
        const { player, playCard } = this.props;
        const { health, activeMana, totalMana, deck, hand, cardsDisabled } = player;

        return (
            <div className="playerContainer">
                <div className="deckPart">
                    <Deck remainingCardCount={deck.length} />
                </div>
                <div className="handPart">
                    <PlayerHand hand={hand} playCard={(cardValue) => playCard(cardValue)} cardsDisabled={cardsDisabled} />
                </div>
                <div className="statsPart">
                    <PlayerStats health={health} activeMana={activeMana} totalMana={totalMana} />
                </div>
            </div>
        );
    }
}