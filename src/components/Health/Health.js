import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Health.css';

export default class Health extends Component {
    static propTypes = {
        health: PropTypes.number
    };

    render() {
        return (
            <div>
                <p>Health: {this.props.health}</p>
            </div>
        );
    }
}