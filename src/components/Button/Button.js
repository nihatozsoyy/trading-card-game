import React from "react";
import PropTypes from "prop-types";
import "./Button.css";

export default class Button extends React.Component {
    static propTypes = {
        text: PropTypes.string,
        clickHandler: PropTypes.func,
        isDisabled: PropTypes.bool
    };

    render() {
        const { text, clickHandler, isDisabled } = this.props;
        return (
            <div>
                <button onClick={clickHandler} disabled={isDisabled} className='button'>{text}</button>
            </div>
        );
    }
}
