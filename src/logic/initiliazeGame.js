import * as Constants from '../Constants';

const deck = Constants.initialDeck;
const health = Constants.initialHealth;
const activeMana = Constants.initialActiveManaSlots
const totalMana = Constants.initialTotalManaSlots

function setInitialCards() {
    const shuffledDeck = [...deck].sort(() => 0.5 - Math.random()); // shuffle deck without changing the original array
    const initialHand = shuffledDeck.splice(0, Constants.initialHandCardCount) // get first n cards from shuffled deck

    return {
        deck: shuffledDeck,
        hand: initialHand
    }
}

export default function initiliazeGame() {
    const user = {
        health,
        activeMana,
        totalMana,
        cardsDisabled: false
    };

    const computer = {
        health,
        activeMana,
        totalMana,
        cardsDisabled: true
    };

    const userCards = setInitialCards(true);
    user.hand = userCards.hand;
    user.deck = userCards.deck;

    const computerCards = setInitialCards(false);
    computer.hand = computerCards.hand;
    computer.deck = computerCards.deck;

    return {
        user,
        computer,
        activePlayerName: '',
        winner: ''
    };
}