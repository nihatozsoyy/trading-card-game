// Select first player randomly and starts the game
export function startGame(state) {
    const playerNames = ['user', 'computer'];
    const activePlayerName = playerNames[Math.floor(Math.random() * playerNames.length)];
    state.activePlayerName = activePlayerName;
    return changeActivePlayer(state);
}

export function playCard(state, manaCost) {
    const activePlayerName = state.activePlayerName;
    const opponentPlayerName = state.activePlayerName === 'user' ? 'computer' : 'user';

    const activePlayer = state[activePlayerName];
    const opponentPlayer = state[opponentPlayerName];

    if (activePlayer.activeMana < manaCost) return state; // return if user does not have enough mana to play the card

    spendMana(activePlayer, manaCost); // spends as many mana as the card has
    damagePlayer(opponentPlayer, manaCost); // damanage the opponent player as many mana as the card has

    // set the winner if opponent player health is zero after damage
    if (opponentPlayer.health === 0) { 
        state.winner = activePlayerName;
        state.user.cardsDisabled = true;
        return state;
    }

    removeCard(activePlayer, manaCost); //removes the played card from hand

    // sets current user and computer states
    state[activePlayerName] = activePlayer;
    state[opponentPlayerName] = opponentPlayer;

    const playableCards = getPlayableCards(activePlayer);
    if (playableCards.length === 0 && activePlayerName === 'user') return changeActivePlayer(state);
    else return state;
}

// Changes active player and prepare for round
export function changeActivePlayer(state) {
    state.activePlayerName = state.activePlayerName === 'user' ? 'computer' : 'user';
    const activePlayer = state[state.activePlayerName];
    refillMana(activePlayer);

    if (activePlayer.totalMana !== 1) {
        if (activePlayer.deck.length !== 0) {
            drawCardFromDeck(activePlayer);
        } else {
            damagePlayer(activePlayer, 1);
        }
    }

    if (state.activePlayerName === 'computer') return playComputer(state);

    if (activePlayer.activeMana < getMinimumPlayableCardMana(activePlayer)) {
        return changeActivePlayer(state);
    } else {
        return state;
    }
}

// Plays the best possible cards for computer
export function playComputer(state) {
    const activePlayer = state.computer;
    let playableCards = getPlayableCards(activePlayer);

    if (playableCards.length > 0) {
        const bestPlayableCard = getBestPlayableCard(playableCards);
        state = playCard(state, bestPlayableCard);
        return playComputer(state);
    } else {
        return changeActivePlayer(state);
    }
}

function spendMana(player, manaCost) {
    player.activeMana -= manaCost;
}

function damagePlayer(player, manaCost) {
    player.health -= manaCost;
    if (player.health < 0) player.health = 0;
}

function removeCard(player, manaCost) {
    const cardIndex = player.hand.indexOf(manaCost);
    player.hand.splice(cardIndex, 1);
}

function refillMana(player) {
    if (player.totalMana !== 10) player.totalMana += 1;
    player.activeMana = player.totalMana;
}

function drawCardFromDeck(player) {
    const newCards = getRandomCardFromDeck(player.deck, player.hand, 1);
    player.deck = newCards.deck;
    player.hand = newCards.hand;
}

function getMinimumPlayableCardMana(player) {
    return Math.min.apply(Math, player.hand);
}

function getBestPlayableCard(playableCards) {
    return Math.max.apply(Math, playableCards);
}

function getPlayableCards(player) {
    return player.hand.filter(mana => mana <= player.activeMana);
}

function getRandomCardFromDeck(deck, hand, cardCount) {
    const newDeck = [...deck].sort(() => 0.5 - Math.random()); // shuffle deck
    hand = hand.concat(newDeck.splice(0, cardCount)) // get first n cards from shuffled deck and combine with current hand

    // Discard new cards if current hand has more than 5 cards
    if (hand.length > 5) hand = hand.splice(0, 5);

    return {
        deck: newDeck,
        hand,
    }
}