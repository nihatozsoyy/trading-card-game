import React from 'react'
import { render, screen } from '@testing-library/react'
import Deck from '../components/Deck/Deck'

test('displays number of remaining cards in deck', () => {
    render(<Deck remainingCardCount={10} />)
    expect(screen.getByText(/^Remaining Cards/)).toHaveTextContent('10')
})