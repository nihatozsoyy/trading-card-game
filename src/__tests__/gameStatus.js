import React from 'react'
import { render, screen } from '@testing-library/react'
import GameStatus from '../components/GameStatus/GameStatus'

test('displays active player name', () => {
    render(<GameStatus activePlayerName={'computer'} />)
    expect(screen.getByText(/^Active Player/)).toHaveTextContent('computer')
})

test('displays winner', () => {
    render(<GameStatus winner={'computer'} />)
    expect(screen.getByText(/^Winner/)).toHaveTextContent('computer')
})