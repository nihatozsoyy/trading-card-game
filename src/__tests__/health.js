import React from 'react'
import { render, screen } from '@testing-library/react'
import Health from '../components/Health/Health'

test('displays player health', () => {
    render(<Health health={10}/>)
    expect(screen.getByText(/^Health/)).toHaveTextContent('10')
})