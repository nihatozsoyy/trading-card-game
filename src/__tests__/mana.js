import React from 'react'
import { render, screen } from '@testing-library/react'
import Mana from '../components/Mana/Mana'

test('displays player mana', () => {
    render(<Mana activeMana={5} totalMana={7}/>)
    expect(screen.getByText(/^Mana/)).toHaveTextContent('5 / 7')
})