import React from 'react'
import { render } from '@testing-library/react'
import Card from '../components/Card/Card'

test('displays cards mana cost', () => {
    const { queryByText } = render(<Card value={5} />)
    expect(queryByText('5')).toBeTruthy()
})

test('makes card disabled', () => {
    const { getByText } = render(<Card isDisabled={true} value={5} />)
    expect(getByText(/5/i).closest('button')).toBeDisabled();
})