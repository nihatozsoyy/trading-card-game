export const initialHealth = 30;
export const initialTotalManaSlots = 0;
export const initialActiveManaSlots = 0;
export const initialDeck = [0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 8];
export const initialHandCardCount = 3;