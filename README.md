This project is created with create-react-app. 
Used react-test-library for testing, and eslint for linter.

Since there is no animation and waiting between the moves in the game, the moves take place instantly and game flows fast.
Therefore, if the first player is computer, it may seem like the game starts with the wrong number of cards, health and mana.

### How to Start?

1) npm install
2) npm start

Nihat Özsoy / nihatozsoyy@gmail.com